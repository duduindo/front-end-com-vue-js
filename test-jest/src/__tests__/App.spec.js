import { mount } from '@vue/test-utils'
import App from '../App.vue'

describe('App.vue', () => {

  test('Should contain Hello', () => {
    const wrapper = mount(App)

    expect(wrapper.text()).toContain('Hello, World!')
  })

  test('toggles message when button is clicked', () => {
    const wrapper = mount(App)
    const button = wrapper.find('button')
    const p = wrapper.find('p')

    button.trigger('click')
    expect(p.text()).toBe('Message!')

    button.trigger('click')
    expect(p.text()).toBe('Toggle message!')

    button.trigger('click')
    expect(p.text()).toBe('Message!')
  })
})
