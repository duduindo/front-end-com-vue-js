import Vue from 'vue'
import App from './App.vue'
import Plugin from './plugin'

Vue.use(Plugin) // created executado pelo plugin

Vue.ola() // Olá mundo do Vue com método global

new Vue({
  el: '#app',
  render: h => h(App)
})
