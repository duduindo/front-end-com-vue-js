
const OlaMundo = {}

OlaMundo.install = function(Vue, options) {
  Vue.ola = () => {
    console.warn('Olá mundo do Vue com método global')
  }

  Vue.prototype.$ola = options => {
    console.warn('Olá mundo do Vue com método de instância')
  };

  Vue.directive('ola', {
    bind(el, binding, vnode, oldVnode) {
      el.innerHTML = 'texto do plugin'
    }
  })


  Vue.mixin({
    created: () => {
      console.warn('created executado pelo plugin');
    }
  })
}


export default OlaMundo
