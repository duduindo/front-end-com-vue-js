export default {
  bind: function(elemento, informacao, vnode) {
    const modifier = Object.keys(informacao.modifiers)[0];

    switch( modifier ) {
      case 'maiusculo':
        elemento.innerHTML = elemento.innerHTML.toUpperCase()
        break

      case 'minusculo':
        elemento.innerHTML = elemento.innerHTML.toLowerCase()
        break

      case 'capitalizado':
        const text = elemento.innerHTML.split(' ')
        elemento.innerHTML = '';

        for (let i = 0; i < text.length; i++)
          elemento.innerHTML += `${text[i].substring(0, 1).toUpperCase()}${text[i].substring(1)} `
        break
    }
  }
}
