


### CAPÍTULO 	4
#### CRIANDO	E	EXIBINDO DADOS

Pasta: `criando-exibindo-dados`



### CAPÍTULO 5 
#### MANIPULANDO DADOS

Pasta: `manipulando-dados`


##### Desempenho: Filters vs Computed

Podemos concluir que os filtros são sempre reexecutados,
enquanto as computed properties são chamadas apenas quando o
atributo que elas representam sofre alguma alteração. Logo, elas
são melhores no desempenho.


##### Watch
ARMAZENE NA MENTE
Note que nosso watch e a variável que queremos observar
possuem o mesmo nome. Isso é um requisito para que
possamos ter sucesso na operação, pois assim o Vue vincula
tal variável a tal observador, fazendo então o citado
observador personalizado.


### CAPÍTULO 6 
#### COMPONENTES JUNTOS SÃO MAIS FORTES

Pasta: `time-futebol`


### CAPÍTULO 7
#### REUTILIZANDO COMPONENTES

Pasta: `chapter-7`


### CAPÍTULO 8
#### CADA UM SEGUE SEU CAMINHO, COM ROTAS!

Pasta: `rotas`


DEFINA 'API HISTORY '
É uma API fornecida pelo HTML5. Ela permite que, via
JavaScript, manipulemos diretamente o histórico do
navegador, adicionando links e trocando de URL sem o
recarregamento (onde a mágica acontece).


##### Obversação Servidor:
Mas para que isso funcione em ambiente de produção, temos
de adicionar um arquivo de configuração no servidor.
Posteriormente, neste livro (capítulo Alguns recursos escondidos),
veremos como publicar nosso site na internet. Lá você encontrará
o tal arquivo de configuração, já que agora ele não é importante
para nós.


### CAPÍTULO 9
#### GERENCIAMENTO DE ESTADO COM VUEX

Pasta: `loja-de-dados`

![](markdown-images/figura-9.8-fluxo-do-vuex.png)


CAPÍTULO 9 ─ GERENCIAMENTO DE ESTADO COM VUEX
9.1 STORE — CRIANDO NOSSA LOJA DE DADOS
9.2 STATES — DECLARANDO DADOS
9.3 MUTATIONS — ALTERANDO DADOS
9.4 GETTERS — PEGANDO DADOS
9.5 ACTIONS — EXECUTANDO MUTAÇÕES INDIRETAMENTE
9.6 MODULES — ORGANIZANDO INFORMAÇÕES
9.7 MAPEANDO ESTADO EM COMPONENTES


### CAPÍTULO 10
#### Criando e dividindo serviços

Pasta: `axios`

### CAPÍTULO 11 - Acrescentando funcionalidades
#### Acrescentando funcionalidades

Pasta: `chapter-11`

Para evitar que a página se recarregue ao dar submit em um formulário, use: `v-on:click.prevent` ou `@click.prevent`

```html
<form action=".">
  <button @click.prevent="metodoClick">Enviar</button>
</form>
```



11.1 CRIANDO DIRETIVAS CUSTOMIZADAS

11.2 CRIANDO SEUS PRÓPRIOS PLUGINS


### CAPÍTULO 12
#### Introdução a testes

Pasta: `testes`


### CAPÍTULO 13
#### ALGUNS RECURSOS ESCONDIDOS

CAPÍTULO 13 ─ ALGUNS RECURSOS ESCONDIDOS
13.1 MANIPULANDO TECLAS DE ATALHO

Pasta: `chapter-13`


##### 13.2 CICLO DE VIDA DOS COMPONENTES

https://br.vuejs.org/v2/guide/instance.html#Diagrama-do-Ciclo-de-Vida


##### 13.3 ESTENDENDO COMPONENTES COM EXTENDS

```html
<script>
import  LvB from  './B.vue';

export  default {
  extends:  LvB,
  data()  { return  { descricao:  'Oi tudo  bem?' } },
};
</script>
```


##### 13.4 TRABALHANDO COM REFERÊNCIAS



##### 13.5 VARIÁVEL CIFRÃO


* `$data` representa `data()`
* `$props`
* `$slots`
* `$refs`
* `$parent` armazena parentes diretos do componente atual na árvore do DOM
* `$root` indica a instância raiz do Vue, aquela mesma que geralmente é colocada no arquivo `main.js`
* `$children` representa todos os filhos de um component
* `$el`
* `$options`


##### 13.6 ATUALIZANDO UM COMPONENTE

##### 13.7 DIRETIVA V-PRE

A diretiva `v-pre` é usada para ignorar a interpolação do Mustache.


##### 13.8 ACESSANDO UM ÍNDICE NO V-FOR


#### 13.9 MODO HISTORY EM PRODUÇÃO

Configurar o modo *history* no Apache e eliminando a hash no **Vue-route**.

https://router.vuejs.org/guide/essentials/history-mode.html#example-server-configurations


#### 13.10 Concluindo

Lembre-se de que todos os exemplos vistos neste livro podem ser encontrados em https://github.com/leonardovilarinho/livro-vue.

E todas as respostas dos exercícios vistos na Parte I foram disponibilizadas em https://github.com/leonardovilarinho/vue-livro-avanco.


### NÃO É DO LIVRO
#### Tests com Jest

Pasta: `test-jest`

NÃO É DO LIVRO - Tests com Jest


