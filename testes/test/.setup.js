require('browser-env')();

const hook = require('require-extension-hooks');

hooks('vue').plugin('vue').push();
hooks(['vue', 'js']).plugin('babel').push();
