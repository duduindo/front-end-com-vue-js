import test from 'ava'
import { mount } from 'avoriaz'
import App from './../src/App.vue'

const componente = mount(App);

test('Total sendo iniciado com valor 0', t => {
  t.is( componente.data().total, 0 )
})


test('Subtraindo um número do total', t => {
  const button = componente.find('button')[0]

  button.trigger('click')

  t.is(componente.data().total, -1 )
})



test('somando um número do total', t => {
  const button = componente.find('button')[0]

  button.trigger('click')

  t.is(componente.data().total, 0 )
})
